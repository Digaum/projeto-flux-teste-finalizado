﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionsController : MonoBehaviour {

    private Animator anin;
    private GameObject robo;

    public GameObject magic;
    public GameObject MagicPoint;
    private GameObject MagicCreated;
    public bool criandoMagic = false;

    private void Start()
    {
        anin = GetComponent<Animator>();
        robo = GameObject.Find("Robo");
    }

    public void Idle()
    {
        if (!criandoMagic)
        {
            anin.SetInteger("AninStatus", 0);
        }

    }

    public void Walk()
    {
        if (anin.GetInteger("AninStatus") != 5)
        {
            anin.SetInteger("AninStatus", 1);
        }
    }

    public void Run()
    {
        if (anin.GetInteger("AninStatus") != 5)
        {
            anin.SetInteger("AninStatus", 2);
        }
    }

    public void Jump()
    {
        if (anin.GetInteger("AninStatus") != 5)
        {
            anin.SetInteger("AninStatus", 3);
        }

    }

    public void Attack()
    {
        if (anin.GetInteger("AninStatus") != 5)
        {
            anin.SetInteger("AninStatus", 4);
        }
    }

    public void Magic()
    {
        if (anin.GetInteger("AninStatus") != 5 && !criandoMagic)
        {
            criandoMagic = true;
            anin.SetInteger("AninStatus", 5);
        }
    }

    public void JumpPula()
    {
        if (anin.GetInteger("AninStatus") != 5)
        {
            robo.GetComponent<PlayerController>().JumpPula();
        }

    }

    public void CriaMagic()
    {
        MagicCreated = Instantiate(magic, MagicPoint.transform.position, MagicPoint.transform.rotation);
    }

    public void SoltaMagic()
    {
        MagicCreated.GetComponent<Magic>().StatusMagic = true;
        criandoMagic = false;
    }
}
