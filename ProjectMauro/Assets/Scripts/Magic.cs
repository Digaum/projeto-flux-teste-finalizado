﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magic : MonoBehaviour {

    public float force;
    private bool statusMagic = false;

    public GameObject magicPoint;

    public bool StatusMagic
    {
        set
        {
            statusMagic = value;
        }
    }

    // Use this for initialization
    void Start () {

        magicPoint = GameObject.Find("MagicPoint");
        Destroy(gameObject, 10);

    }

    private void FixedUpdate()
    {
        SoltaMagia();
    }

    public void SoltaMagia()
    {
        if (statusMagic)
        {
            GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * force * Time.deltaTime);

            Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, 0, 500) * Time.deltaTime);
            GetComponent<Rigidbody>().MoveRotation(GetComponent<Rigidbody>().rotation * deltaRotation);
        }
        else
        {
            transform.position = new Vector3(magicPoint.transform.position.x,transform.position.y,transform.position.z);
        }
    }
}
