﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    public CharacterController ccRobo;
    public GameObject robotKyle;

    Vector3 direcao = new Vector3(0, 0, 0);
    public float jumpForce;
    private float gravidade = 20f;
    public bool estaNoChao;

    void Start () {

        robotKyle = GameObject.Find("Robot Kyle");
        ccRobo = GetComponent<CharacterController>();

	}
	
	void Update () {

        ccRobo.SimpleMove(Physics.gravity);
        estaNoChao = ccRobo.isGrounded;
        direcao.y -= gravidade * Time.deltaTime;
        ccRobo.Move(direcao * Time.deltaTime);


    }

    public void Idle()
    {
        if (ccRobo.isGrounded)
        {
            robotKyle.GetComponent<ActionsController>().Idle();
        }

    }

    public void Walk()
    {
        if (ccRobo.isGrounded)
        {
            robotKyle.GetComponent<ActionsController>().Walk();
        }

    }

    public void Run()
    {
        if (ccRobo.isGrounded)
        {
            robotKyle.GetComponent<ActionsController>().Run();
        }

    }

    public void JumpPrepara()
    {
        if (ccRobo.isGrounded)
        {
            robotKyle.GetComponent<ActionsController>().Jump();
        }      
    }

    public void JumpPula()
    {
        if (ccRobo.isGrounded)
        {
            direcao.y = jumpForce;
        }
    }

    public void Attack()
    {
        if (ccRobo.isGrounded)
        {
            robotKyle.GetComponent<ActionsController>().Attack();
        }
    }

    public void Magic()
    {
        if (ccRobo.isGrounded)
        {
            robotKyle.GetComponent<ActionsController>().Magic();
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
