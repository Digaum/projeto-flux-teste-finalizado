﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private const float Y_ANGLE_MIN = 0.0f;
    private const float Y_ANGLE_MAX = 50.0f;

    public Transform lookAt;
    public Transform camTransform;
    public float distance = 10.0f;

    private float currentX = -130.0f;
    private float currentY = 20.0f;
    private float sensitivityX = 2.0f;
    private float sensitivityY = 2.0f;

    private void Start()
    {
        camTransform = transform;
    }

    private void Update()
    {

        currentX -= Input.GetAxis("Horizontal") * sensitivityX;
        currentY += Input.GetAxis("Vertical") * sensitivityY;

        currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);

        /*Debug.Log("CURRENTX: " + currentX);
        Debug.Log("CURRENTY: " + currentY);*/
    }

    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTransform.position = lookAt.position + rotation * dir;
        camTransform.LookAt(lookAt.position);
    }
}
